import "bootstrap/dist/css/bootstrap.min.css";
import ApiProvider from "./context/ApiContext";
import AuthProvider, { AuthContext } from "./context/AuthContext";
import LayoutMain from "./layout";
import Navigation from "./routes/Navigation";
import { getLocalStorage } from "./utils/localStorage";
//Revisar si tenemos un usuario en local Storage


function App() {
  const user = getLocalStorage('usuario');
  const isLogged = getLocalStorage('isLogged');
  return (
    <AuthProvider>
      <ApiProvider>
        <LayoutMain>
          <Navigation  user={user} logged={isLogged}/>
        </LayoutMain>
      </ApiProvider>
    </AuthProvider>
  );
}

export default App;
