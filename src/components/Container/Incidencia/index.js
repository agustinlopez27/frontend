import React, { useState, useContext, useEffect } from "react";
import InputField from "../../shared/Input";
import InputFile from "../../shared/InputFile";
import { Button, Modal, Spinner, Row, Col, Alert } from "react-bootstrap";
import { ApiContext } from "../../../context/ApiContext";
import flagMexico from "../../../assets/img/paises/mexico.png";
import flagColombia from "../../../assets/img/paises/colombia.png";
import flagPeru from "../../../assets/img/paises/peru.png";
import flagEua from "../../../assets/img/paises/eua.png";

import styles from "./Layout.module.css";
import ModalShared from "../../shared/Modal";
const Incidencia = () => {
  const { onFileUpload, isloading, error, setError } = useContext(ApiContext);

  const [numeroTelefono, setNumeroTelefono] = useState("");
  const [codigoPais, setCodigoPais] = useState("");
  const [selectedFile, setSelectedFile] = useState(null);
  const handleChangeData = (key, value) => {
    const re = /^[0-9]+$/;
    if (value === "" || re.test(value)) {
      setNumeroTelefono(value);
    }
  };
  const handleChangeCode = (key, value) => {
    const re = /^[0-9+]+$/;
    if (value === "" || re.test(value)) {
      setCodigoPais(value);
    }
  };

  const [smShow, setSmShow] = useState(false);

  const handleChangeFile = (value) => {
    setSelectedFile(value);
  };

  const dataToSend = {
    selectedFile,
    numeroTelefono,
    codigoPais,
  };

  let imageCountry = null;
  let errorCode = null;
  const switchCountry = (code) => {
    switch (code) {
      case "52":
        return (imageCountry = flagMexico);
      case "57":
        return (imageCountry = flagColombia);
      case "51":
        return (imageCountry = flagPeru);
      case "1":
        return (imageCountry = flagEua);
      default:
        return "";
    }
  };

  const test = () => {
    if (codigoPais.length >= 1) {
      //get the first element
      let firstLetter = codigoPais.charAt(0);
      if (firstLetter === "+") {
        let str = codigoPais.substring(1, 3);
        if (str === "1") {
          imageCountry = switchCountry(str);
        }
        if (str !== "1" && str.length === 2) {
          imageCountry = switchCountry(str);
        }
      } else {
        // /setCodeError(null)
        errorCode = "No cumple el formato por favor intente como ejemplo: +52";
      }
    }
  };

  test();

  return (
    <div className={styles.containerInc}>
      <h2 className={styles.styleFonts}>Registro de incidencias</h2>
      <div>
        <Row>
          <Col sm={12} md={2}>
            <InputField
              value={codigoPais}
              label="Cod. Pais"
              placeholder="Ej +52"
              type="text"
              getValue={(e) => handleChangeCode("codigoPais", e)}
              maxLength={3}
            >
              {" "}
              <div className={styles.flexFlag}>
                {imageCountry && (

                    <img
                    className={styles.fadeIn}
                      src={imageCountry}
                      style={{ maxWidth: "30px" }}
                      alt={imageCountry}
                    />
                )}
              </div>
            </InputField>
          </Col>
          <Col sm={12} md={10}>
            <InputField
              value={numeroTelefono}
              label="Numero de telefono"
              placeholder="Ingresa el Número de telefono con código de país"
              type="text"
              getValue={(e) => handleChangeData("numeroTelefono", e)}
              maxLength={10}
            ></InputField>
          </Col>
          {errorCode && (
            <div className={styles.fadeIn}>
              <Alert variant="danger">
                <p className="mb-0">{errorCode}</p>
              </Alert>
            </div>
          )}
        </Row>
      </div>

      <div>
        <InputFile
          // value={selectedFile?.name}
          placeholder="Sube un archivo"
          getValue={(e) => handleChangeFile(e)}
          accept=".csv"
        />
      </div>

      <Button
        onClick={() =>
          onFileUpload(dataToSend, setNumeroTelefono, setSelectedFile)
        }
        disabled={errorCode}
      >
        {" "}
        Enviar información
      </Button>

      {isloading && (
        <>
          <Modal
            size="sm"
            show={isloading}
            onHide={() => setSmShow(false)}
            aria-labelledby="example-modal-sizes-title-sm"
          >
            <Modal.Header>
              <Spinner animation="border" variant="warning" />
            </Modal.Header>
          </Modal>
        </>
      )}
      {error && (
        <>
          <ModalShared
            show={error}
            onHide={setError}
            message={error}
            type="Error"
          />
        </>
      )}
    </div>
  );
};

export default Incidencia;
