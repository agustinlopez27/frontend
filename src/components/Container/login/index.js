import React from 'react'
import Login from '../../auth/login'
import styles from './styles.module.css'
import imgMC from '../../../assets/img/mobileCard.png'
import { Container } from 'react-bootstrap'
const MainLogin = () => {
  return (
    <div className={styles.containerFlex}>
      <Container>
            <img src={imgMC} />
      </Container>
      <Container>
        <Login />
      </Container>
    </div>
  );
};

export default MainLogin
