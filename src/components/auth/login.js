import React, { useState, useContext, useEffect } from "react";
import InputField from "../shared/Input";
import MyButton from "../shared/Button";
import { arrayUser } from "../../constants";
import { useHistory } from 'react-router-dom';
import { AuthContext } from "../../context/AuthContext";
import styles from './stylesAuth.module.css'
const initialState = {
    nombre: "",
    password: "",
}
const Login = () => {
  
  const [objeto, setObjeto] = useState(initialState);
  const {loginSubmit,isLogged} = useContext(AuthContext)
  const history = useHistory();
  useEffect(() => {
    if(isLogged){
      history.push("/incidencias");
    }
  }, [isLogged])
  const handleChangeData = (key, value) => {
    setObjeto({
      ...objeto,
      [key]: value,
    });
  };

  return (
    <div className={styles.containerAuth}>
      <h2 className={styles.styleFontsAuth}>Bienvenido por favor ingrese sus datos!</h2>
      <InputField
        value={objeto.nombre}
        label="E-mail"
        placeholder="Ingresa tu E-mail"
        type="email"
        getValue={(e) => handleChangeData("nombre", e)}
      />
      <InputField
        value={objeto.password}
        label="Password"
        placeholder="Ingresa tu password"
        type="password"
        getValue={(e) => handleChangeData("password", e)}
      />

      
      <MyButton
        variant="primary"
        size="lg"
        onClick={() => loginSubmit(objeto)}
        text="Login"
      />
    </div>
  );
};

export default Login;
