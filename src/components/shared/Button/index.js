import React from 'react'
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';


const MyButton = ({text, disabled, variant, size, onClick,...props}) => {
    
  return (
    <div className="mb-2">
      <Button variant={variant} size={size} onClick={onClick}>
        {text}
      </Button>
    </div>
  );
};


MyButton.defaultProps = {
    text: 'Click',
    variant: 'primary'
  };

MyButton.propTypes = {
    text: PropTypes.string,
    variant: PropTypes.string,
    size: PropTypes.string,
  };
  

export default MyButton
