import React from 'react';
import PropTypes from 'prop-types';
import { InputGroup, Form, FormControl,Row,Col } from 'react-bootstrap';

const InputField = ({
  value,
  label,
  name,
  placeholder,
  type,
  getValue,
  maxLength,
  showMask,
  titleMask,
  children
}) => {

    const handleOnChange = (e) => {
      const { value } = e.target;
      if (getValue) {
        maxLength
          ? getValue(
              value.length > maxLength ? value.slice(0, maxLength) : value
            )
          : getValue(value);
      }
    };


  return (
      <Row>
      {label && <Col md={12}>
      <Form.Label htmlFor="basic-url">{label}</Form.Label></Col>}
      <InputGroup className="mb-3">
        {children && children}
        {showMask && <InputGroup.Text id="basic-addon1">{titleMask}</InputGroup.Text>}
        <FormControl
          type={type}
          value={value}
          name={name}
          className="form-control"
          placeholder={placeholder}
          onChange={handleOnChange}
          maxLength={maxLength}
        />
      </InputGroup>
      </Row>
  );
};

InputField.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string.isRequired,
  getValue: PropTypes.func,
  maxLength: PropTypes.number,
  showMask: PropTypes.bool,
  titleMask: PropTypes.string
};

export default InputField
