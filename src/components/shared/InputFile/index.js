import React from "react";
import { InputGroup, Form, FormControl } from "react-bootstrap";

const InputFile = ({
  value,
  label,
  name,
  placeholder,
  getValue,
  showMask,
  titleMask,
  accept
}) => {
  const handleOnChange = (e) => {
    const file = e.target.files[0];
    getValue(file)
  };

  return (
    <>
      {label && <Form.Label htmlFor="basic-url">{label}</Form.Label>}
      <InputGroup className="mb-3">
        {showMask && (
          <InputGroup.Text id="basic-addon1">{titleMask}</InputGroup.Text>
        )}
        <FormControl
          type="file"
          className="form-control"
          placeholder={placeholder}
          onChange={handleOnChange}
          accept={accept}
        />
      </InputGroup>
    </>
  );
};

export default InputFile;
