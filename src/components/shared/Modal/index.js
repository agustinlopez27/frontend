import React from "react";
import { Modal } from "react-bootstrap";
const ModalShared = ({ show, onHide, message, type }) => {
  return (
    <Modal
      size="sm"
      show={show}
      onHide={() => onHide(null)}
      aria-labelledby="example-modal-sizes-title-sm"
    >
      <Modal.Dialog>
        <Modal.Header closeButton>
          <Modal.Title>
            {type === "Error" ? "Error en la peticion" : "Exitoso!"}
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <p>{message}</p>
        </Modal.Body>
      </Modal.Dialog>
    </Modal>
  );
};

export default ModalShared;
