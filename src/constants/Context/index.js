//Types para el login
export const LOGIN = 'LOGIN';
export const LOGIN_EXITOSO = 'LOGIN_EXITOSO';
export const LOGIN_ERROR = 'LOGIN_ERROR';
//TYPES para el servicio de disable
export const DISABLE_USER = 'DISABLE_USER';
export const DISABLE_USER_EXITOSO = 'DISABLE_USER_EXITOSO';
export const DISABLE_USER_ERROR = 'DISABLE_USER_ERROR';
//TYPES SUMBIT INPUT
export const SUBMIT_INPUT = 'SUBMIT_INPUT ';
export const SUBMIT_INPUT_EXITOSO = 'SUBMIT_INPUT _EXITOSO';
export const SUBMIT_INPUT_ERROR = 'SUBMIT_INPUT _ERROR';
//TYPES SUMBIT FILE
export const SUBMIT_FILE = 'SUBMIT_FILE ';
export const SUBMIT_FILE_EXITOSO = 'SUBMIT_FILE _EXITOSO';
export const SUBMIT_FILE_ERROR = 'SUBMIT_FILE _ERROR';