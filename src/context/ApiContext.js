import React, { createContext, useState } from "react";
import axios from "axios";
import { baseUlr } from "../constants";
//Creacion del Context
export const ApiContext = createContext();

const ApiProvider = (props) => {
  const [isloading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

  const onFileUpload = async (data, setNumeroTelefono) => {
    const { selectedFile, numeroTelefono, codigoPais } = data;
    console.log(data);
    const numberphone = codigoPais + numeroTelefono;
    const cleanNumber = numberphone.replace("+","");

    if (numeroTelefono && codigoPais) {
      if(cleanNumber.length >= 11) {
        try {
          setIsLoading(true);
          const resp = await axios.post(
            "https://1sx9iooqk2.execute-api.us-west-2.amazonaws.com/dev/input",
            cleanNumber
          );
          if (resp.data.statusCode !== 200) {
            setNumeroTelefono("");
            setIsLoading(false);
            setError(resp.data.body);
          } else {
            setIsLoading(false);
            setNumeroTelefono("");
          }
        } catch (error) {
          console.log(error, "<=== Error");
          setError(error);
        }
      }else{
        setError(`El numero de caracteres debe ser de 11 o 12 usted ingreso ${numeroTelefono.length }`);
      }
    }else{
      setError(`No deje campos vacios`);
    }





    if (selectedFile) {
      setIsLoading(true);
      let formData = new FormData();
      formData.append("file", selectedFile);
      try {
        await axios.post(baseUlr, formData);
        setIsLoading(false);
      } catch (error) {
        console.log(error, "<==== Error");
      }
    }
  };
  return (
    <ApiContext.Provider
      value={{ onFileUpload, isloading, error, setError, success, setSuccess }}
    >
      {props.children}
    </ApiContext.Provider>
  );
};


export default ApiProvider;
