import React, {createContext,  useState} from 'react';
import { arrayUser } from '../constants';
import { getLocalStorage, setInLocalStorage } from '../utils/localStorage';

export const AuthContext = createContext();

const AuthProvider = (props) => {
    const [isLogged, setIsLogged] = useState(false)
    const user = getLocalStorage('usuario')
    const loginSubmit = (data) => {
        const dataUser = arrayUser.filter(item => item.email === data.nombre && item.password === data.password);
        if(dataUser.length > 0 || user) {
            alert("Bienvenido!")
            setInLocalStorage('usuario', dataUser[0].email)
            setInLocalStorage('isLogged', true)
            setIsLogged(true)
        }else{
            alert("No puedes acceder al sistema");
            setIsLogged(false)
        }
    }

    const cerrarSesion = () => {
        localStorage.clear();
        window.location.pathname = "/"
    }
    
    return(
        <AuthContext.Provider value={{isLogged, loginSubmit, setIsLogged, cerrarSesion}}>
            {props.children}
        </AuthContext.Provider>
    )
}

export default AuthProvider;