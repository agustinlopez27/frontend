import React, { useContext } from "react";
import { Navbar, Container, Row, Col, Button } from "react-bootstrap";
import logo from "../assets/img/mobileCard.png"
import { AuthContext } from "../context/AuthContext";
import { getLocalStorage } from "../utils/localStorage";
const PrivateLayout = (props) => {
  const auth = useContext(AuthContext);
  const {cerrarSesion} = auth;
  const localuser = getLocalStorage('usuario')
  return (
    <>
      <Row>
        <Col md={12}>
          <Navbar bg="dark" variant="dark">
            <Container>
              <Navbar.Brand href="#home">
                <img
                  alt=""
                  src={logo}
                  width="30"
                  height="30"
                  className="d-inline-block align-top"
                />{" "}
                Mobile Card
              </Navbar.Brand>

              <Navbar.Collapse className="justify-content-center">
                <Navbar.Text>
                  Bienvenido <a href="#login">{localuser}</a>
                </Navbar.Text>
              </Navbar.Collapse>
              <Navbar.Collapse className="justify-content-end">
                <Button variant="light" onClick={() => cerrarSesion()}>Cerrar Sesión</Button>
              </Navbar.Collapse>
            </Container>
          </Navbar>
        </Col>
        <Col md={12}>
        <Container>{props.children}</Container>
        </Col>
      </Row>

     
    </>
  );
};

export default PrivateLayout;
