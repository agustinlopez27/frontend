import { Container, Row, Col } from 'react-bootstrap';
import styles from "./Layout.module.css"
const LayoutMain = (props) => {
  return (
    
    <div className={styles.stylesContainer}>
        {props.children}
     
    </div>
   
  );
};

export default LayoutMain;
