import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Login } from "../components/auth";
import Incidencia from "../components/Container/Incidencia";
import MainLogin from "../components/Container/login";
import PrivateRoute from "./PrivateRoute";

const Navigation = ({user, logged}) => {
  return (
    <Router>
            <Switch>
                <PrivateRoute exact path="/incidencias" component={Incidencia} user={user} logged={logged}/> 
                <Route exact path="/" component={MainLogin} /> 
             </Switch>  
    </Router>
  );
};

export default Navigation;
