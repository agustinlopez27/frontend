import React, {useContext} from 'react';
import {Route, Redirect} from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import PrivateLayout from '../layout/PrivateLayout';


const PrivateRoute = ({component: Component, ...props}) => {
    const {logged, user} = props;
    const auth = useContext(AuthContext);
    return (
        <Route {...props} render={props => !logged && !user  ? (
            <Redirect to="/" />
        ) : (
            <PrivateLayout {...props}>
                <Component {...props}/>
            </PrivateLayout>
        )}/>
        
    )
}

export default PrivateRoute
