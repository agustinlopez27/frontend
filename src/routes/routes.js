import { Login } from "../components/auth";
import Incidencia from "../components/Container/Incidencia";

const routes = [
    {
        path: '/',
        component: Incidencia,
        exact: true,
    },
    {
        path: '/incidencias',
        component: Incidencia,
        exact: true,
    }
]

export default routes;